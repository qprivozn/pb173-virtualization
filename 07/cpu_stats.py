#!/usr/bin/env python

import libvirt
import time
import getopt
import sys

def print_help(progname):
    print(f"{progname} options:\n" +
          "\n" +
          "     -c | --connect=URI          connection URI\n" +
          "     -d | --domain=NAME          domain name\n" +
          "     -s | --sleep=SECONDS        how many seconds sleep between prints\n" +
          "\n")

def parse_argv():
    options = "hc:d:s:"
    long_options = [ "help", "connect=", "domain=", "sleep=" ]
    connect = None
    domain = None
    sleep = 2

    try:
        args, values = getopt.getopt(sys.argv[1:], options, long_options)
        for currArg, currVal in args:
            if currArg in ("-h", "--help"):
                print_help(sys.argv[0])
                sys.exit(0)
            elif currArg in ("-c", "--connect"):
                connect = currVal
            elif currArg in ("-d", "--domain"):
                domain = currVal
            elif currArg in ("-s", "--sleep"):
                try:
                    sleep = int(currVal)
                except ValueError as err:
                    print(f"Invalid number '{currVal}'")
                    sys.exit(1)

    except getopt.error as err:
        print(str(err))
        sys.exit(1)

    if domain is None:
        print("At least domain name must be specified")
        sys.exit(1)

    return connect, domain, sleep

def do_top(conn_uri, dom_name, sleep_time = 2):
    conn = libvirt.open(conn_uri)
    dom = conn.lookupByName(dom_name)
    while 1:
        old = dom.getCPUStats(total=1)[0]['cpu_time']
        time.sleep(sleep_time)
        new = dom.getCPUStats(total=1)[0]['cpu_time']
        #cpu_time is in nanoseconds
        print("CPU: %f" % ((new - old) / 1E9 * 100 / sleep_time))

def main():
    c, d, s = parse_argv()
    do_top(c, d, s)

if __name__ == "__main__":
    sys.exit(main())
